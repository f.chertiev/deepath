/**
The modulbe for processing a requests in Zabbix server-proxy data exchange protocol format
*/
module deepath.formzabbixreq;

import binary.pack;
import vibe.data.json;
import vibe.core.log : logDebug, logInfo, logError, logException;
import std.conv : to;
import deepath.helpers;

/**
The function forming request in Zabbix server-proxy data exchange protocol format.
Params:
  hostname = host name
  key = key of a data element
  message = data in JSON format
Returns:
  Array of bytes with demanded format for sending to Zabbix server/proxy.
References:
  <a href="https://www.zabbix.com/documentation/current/en/manual/appendix/protocols/header_datalen">Zabbix server-proxy data exchange protocol reference</a>
*/
ubyte[] formZabbixReq(string hostname, string key, Json message)
{
    debug { mixin(logFunctionBorders!()); }

    Json body = Json([
        "request": Json("sender data"), "data": serializeToJson([[
            "host": hostname,
            "key": key,
            "value": message.to!string
            ]])
        ]);

    return cast(ubyte[])("ZBXD\x01") ~ pack!`<L`(body.toString.length) ~ cast(ubyte[])(body.toString);
}